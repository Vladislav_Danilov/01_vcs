/*������� ������������ ������� ���������� ��������� ��������������� � ����� ��������*/
package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import prop.PropertiesSession;

public class AbiturInputAdmin extends HttpServlet {
	private static Logger log = Logger.getLogger(AbiturInputAdmin.class);
	private static final long serialVersionUID = 1L;

	public AbiturInputAdmin() {
		super();
	}

	@SuppressWarnings("static-access")
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		log.info("start method");
		String idform = "";
		String idabitur = "";
		String idsubject = "";
		String idfaculty = "";
		/* ������� �������� � �������� ��������� */
		idform = new String(request.getParameter("idform").getBytes(
				"ISO-8859-1"), "UTF-8");
		request.setAttribute("idform", idform);
		idsubject = new String(request.getParameter("idsubject").getBytes(
				"ISO-8859-1"), "UTF-8");
		request.setAttribute("idsubject", idsubject);
		idabitur = new String(request.getParameter("idabitur").getBytes(
				"ISO-8859-1"), "UTF-8");
		request.setAttribute("idabitur", idabitur);
		idfaculty = new String(request.getParameter("idfaculty").getBytes(
				"ISO-8859-1"), "UTF-8");
		request.setAttribute("idfaculty", idfaculty);
		/*
		 * ����� ���������� ������ autorisation ���������� �������� ������ �
		 * ������
		 */
		PropertiesSession setting = new PropertiesSession();
		if ((setting.getLogin() == null) && (setting.getPassword() == null)) {
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		} else {
			request.getRequestDispatcher("administrator.jsp").forward(request,
					response);
		}
		log.info("stop method");
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

}
