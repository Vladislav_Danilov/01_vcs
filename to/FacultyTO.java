/*����� ��������� ���� ������ ������� ����� ����� DAO � ������� ������, �������� ������ � �����������
 * ������������
 */

package to;

public class FacultyTO {
	int id;
	String name_s;
	String middle_name_s;
	String last_name_s;
	String name_f;
	float mark_at;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName_s() {
		return name_s;
	}
	public void setName_s(String name_s) {
		this.name_s = name_s;
	}
	public String getMiddle_name_s() {
		return middle_name_s;
	}
	public void setMiddle_name_s(String middle_name_s) {
		this.middle_name_s = middle_name_s;
	}
	public String getLast_name_s() {
		return last_name_s;
	}
	public void setLast_name_s(String last_name_s) {
		this.last_name_s = last_name_s;
	}
	public String getName_f() {
		return name_f;
	}
	public void setName_f(String name_f) {
		this.name_f = name_f;
	}
	public float getMark_at() {
		return mark_at;
	}
	public void setMark_at(float mark_at) {
		this.mark_at = mark_at;
	}
	public FacultyTO(int id, String name_s, String middle_name_s,
			String last_name_s, String name_f, float mark_at) {
		super();
		this.id = id;
		this.name_s = name_s;
		this.middle_name_s = middle_name_s;
		this.last_name_s = last_name_s;
		this.name_f = name_f;
		this.mark_at = mark_at;
	}

	
}
